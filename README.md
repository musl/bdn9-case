# Bdn9 Case

A 3d-print-able, CNC-mill-able case for keeb.io's BDN9 macro pad.

# Printing

This printed beatifully on a stock Prusa i3 Mk3S with the following
settings:

- 0.45mm nozzle
- 0.2mm layer height
- 0% or no infill for smooth diffusion, it's plenty strong.
- conservative/slow speeds, low volumetric flow rate
- no auto-cooling, fan off on the first layer, 100% the rest of the
	time.

Note that these settings, including a bunch more of my favorites for
higher-quality printing with Hatchbox PLA are included in the `3mf`
Slic3r project file.

I used a well-known brand of white PLA that was nicely translucent. Most
pure-white, off-white, and natural-color filaments are fairly
translucent and will work very well. Their opacity helps hide printing
defects, improves diffusion, and generally looks nicer than "clear"
filaments for this design.

# Milling
 
I don't have a ton of milling experience, but I did my best to make
this case easily millable. The tolerances are not very tight, so it's
probably pretty forgiving. I'd love to see somebody's results with
frosty polycarbonate.

